package me.sirreason.bedrock;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class BedrockBreak extends JavaPlugin implements Listener {

    public void onEnable() {
        saveDefaultConfig();
        getServer().getPluginManager().registerEvents(this, this);
        getLogger().info("has been enabled");
    }

    public void onDisable() {
        getLogger().info("has been disabled");
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBlockDamage(BlockDamageEvent e) {
        if (e.getBlock().getType() != Material.BEDROCK || e.getItemInHand().getType() != Material.DIAMOND_PICKAXE) return;
        if (e.getPlayer().hasPermission("nearpvp.bedrock"))
            if (e.getBlock().getY() >= getConfig().getInt("minY")) {
                BlockBreakEvent event = new BlockBreakEvent(e.getBlock(), e.getPlayer());
                Bukkit.getServer().getPluginManager().callEvent(event);
                if (!event.isCancelled()) e.getBlock().breakNaturally();
            }
    }
}
